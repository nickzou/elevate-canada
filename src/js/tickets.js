import Swiper from 'swiper';
import countDown from './functions/common/count-down';

let slider = new Swiper('#slider', {
    direction: 'horizontal',
    loop: false,
  
    slidesPerView: 5,
	spaceBetween: 15,
	slidesOffsetBefore: 15,
	slidesOffsetAfter: 15,
  
    breakpoints: {
      1399: {
        slidesPerView: 3.25
      },
      1199: {
        slidesPerView: 2.25
      },
      767: {
        slidesPerView: 1.25
      }
    }
});

let groupSlider = new Swiper('#group-slider', {
    direction: 'horizontal',
    loop: false,
  
    slidesPerView: 5,
	spaceBetween: 15,
	slidesOffsetBefore: 15,
  
    breakpoints: {
      1399: {
        slidesPerView: 3.25
      },
      1199: {
        slidesPerView: 2.25
      },
      767: {
        slidesPerView: 1.25
      }
    }
});

let earlybirdTab = document.getElementById('earlybird-tab');
let midsummerTab = document.getElementById('midsummer-tab');
let regularTab = document.getElementById('regular-tab');
let walkupTab = document.getElementById('walkup-tab');

let groupEarlybirdTab = document.getElementById('group-earlybird-tab');
let groupMidsummerTab = document.getElementById('group-midsummer-tab');
let groupRegularTab = document.getElementById('group-regular-tab');
let groupWalkupTab = document.getElementById('group-walkup-tab');

let earlybirdRow = [...document.querySelectorAll('.prices-row.earlybird.single')];
let midsummerRow = [...document.querySelectorAll('.prices-row.midsummer.single')];
let regularRow = [...document.querySelectorAll('.prices-row.regular.single')];
let walkupRow = [...document.querySelectorAll('.prices-row.walkup.single')];

let groupEarlybirdRow = [...document.querySelectorAll('.prices-row.earlybird.group')];
let groupMidsummerRow = [...document.querySelectorAll('.prices-row.midsummer.group')];
let groupRegularRow = [...document.querySelectorAll('.prices-row.regular.group')];
let groupWalkupRow = [...document.querySelectorAll('.prices-row.walkup.group')];

let earlybirdCountdown = earlybirdTab.dataset.countdownValue;
let midsummerCountdown = midsummerTab.dataset.countdownValue;
let regularCountdown = regularTab.dataset.countdownValue;
let walkupCountdown = walkupTab.dataset.countdownValue;

let daysEl = document.querySelector('.hero-container #days');
let hoursEl = document.querySelector('.hero-container #hours');
let minutesEl = document.querySelector('.hero-container #minutes');

let now = new Date().getTime();

if(Date.parse(new Date()) - Date.parse(earlybirdCountdown) < 0) {
	earlybirdTab.classList.add('active');
	groupEarlybirdTab.classList.add('active');
	earlybirdRow.forEach(row=>row.classList.add('active'));
	groupEarlybirdRow.forEach(row=>row.classList.add('active'));
	countDown(earlybirdCountdown, daysEl, hoursEl, minutesEl);
} else if (Date.parse(new Date()) - Date.parse(midsummerCountdown) < 0) {
	earlybirdTab.classList.add('disable');
	midsummerTab.classList.add('active');
	groupEarlybirdTab.classList.add('disable');
	groupMidsummerTab.classList.add('active');
	midsummerRow.forEach(row=>row.classList.add('active'));
	groupMidsummerRow.forEach(row=>row.classList.add('active'));
	countDown(midsummerCountdown, daysEl, hoursEl, minutesEl);
} else if (Date.parse(new Date()) - Date.parse(regularCountdown) < 0) {
	earlybirdTab.classList.add('disable');
	midsummerTab.classList.add('disable');
	regularTab.classList.add('active');
	groupEarlybirdTab.classList.add('disable');
	groupMidsummerTab.classList.add('disable');
	groupRegularTab.classList.add('active');
	regularRow.forEach(row=>row.classList.add('active'));
	groupRegularRow.forEach(row=>row.classList.add('active'));
	countDown(regularCountdown, daysEl, hoursEl, minutesEl);
} else if (Date.parse(new Date()) - Date.parse(walkupCountdown) < 0) {
	earlybirdTab.classList.add('disable');
	midsummerTab.classList.add('disable');
	regularTab.classList.add('disable');
	walkupTab.classList.add('active');
	groupEarlybirdTab.classList.add('disable');
	groupMidsummerTab.classList.add('disable');
	groupRegularTab.classList.add('disable');
	groupWalkupTab.classList.add('active');
	walkupRow.forEach(row=>row.classList.add('active'));
	groupWalkupRow.forEach(row=>row.classList.add('active'));
	countDown(walkupCountdown, daysEl, hoursEl, minutesEl);
}

//countDown(targetTime, daysEl, hoursEl, minutesEl);

let tabButtons = [...document.querySelectorAll('.ticket-purchase-tabs.single')];
let pricesRow = [...document.querySelectorAll('.prices-row.single')];
tabButtons.forEach( button => button.addEventListener('click', function() {
	if(!this.classList.contains('active')) {
		tabButtons.forEach( tab =>tab.classList.remove('active'));
		this.classList.add('active');
		pricesRow.forEach(row=> {
			row.classList.remove('active');
			if(row.dataset.timeFrame === this.dataset.ticketType) {
				row.classList.add('active');
			}
		});
	}
}));

let groupTabButtons = [...document.querySelectorAll('.ticket-purchase-tabs.group')];
let groupPricesRow = [...document.querySelectorAll('.prices-row.group')];
groupTabButtons.forEach( button => button.addEventListener('click', function() {
	console.log(this);
	if(!this.classList.contains('active')) {
		groupTabButtons.forEach( tab =>tab.classList.remove('active'));
		this.classList.add('active');
		groupPricesRow.forEach(row=> {
			row.classList.remove('active');
			if(row.dataset.timeFrame === this.dataset.ticketType) {
				row.classList.add('active');
			}
		});
	}
}));