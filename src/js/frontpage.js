import Swiper from 'swiper';

//inits hero slider
let heroSlider = new Swiper('#slider', {
    direction: 'horizontal',
    loop: true,
    autoplay: {
      delay: 3000,
    },
    navigation: {
      nextEl: '#hero-slider-prev',
      prevEl: '#hero-slider-next',
    },
});

let gallerySlider = new Swiper('#gallery', {
  direction: 'horizontal',
  loop: true,

  slidesPerView: 3,
  spaceBetween: 15,

  navigation: {
    nextEl: '#gallery-slider-next',
    prevEl: '#gallery-slider-prev',
  },

  breakpoints: {
    991: {
      slidesPerView: 2
    },
    767: {
      slidesPerView: 1
    }
  }
});

//inits festival overview slider
let eventsSlider = new Swiper('#events', {
  direction: 'horizontal',
  loop: true,

  slidesPerView: 4,
  spaceBetween: 15,
  centeredSlides: true,

  navigation: {
    nextEl: '#events-slider-next',
    prevEl: '#events-slider-prev',
  },

  breakpoints: {
    1399: {
      slidesPerView: 3
    },
    1199: {
      slidesPerView: 2
    },
    767: {
      slidesPerView: 1
    }
  }
});

//inits video play button
let videoPlayButton;
let videoWrapper = document.getElementById('video-wrapper');
let video = document.getElementById('video');
let videoMethods = {
	renderVideoPlayButton: function() {
		if (videoWrapper.contains(video)) {
			this.formatVideoPlayButton();
			video.classList.add('has-media-controls-hidden');
			videoPlayButton = document.getElementById('play-button');
			videoPlayButton.addEventListener('click', this.hideVideoPlayButton);
		}
	},
	formatVideoPlayButton: function() {
		videoWrapper.insertAdjacentHTML('beforeend', `\
			<svg version="1.1" id="play-button" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve" class="video-overlay-play-button">\
				<style type="text/css">\
					.st0{fill:#E4002B;}\
					.st1{fill:#FFFFFF;}\
				</style>\
				<circle class="st0" cx="150" cy="150" r="150"/>\
				<polygon class="st1" points="205,150 119.1,193 119.1,107 "/>\
			</svg>\
		`);
	},
	hideVideoPlayButton: function() {
		video.play();
		videoPlayButton.classList.add('is-hidden');
		video.classList.remove('has-media-controls-hidden');
		video.setAttribute('controls', 'controls');
	}
}

let body = document.getElementsByTagName('body')[0];
let sliderButtons = document.querySelectorAll('#slider .slide-modal-button');

window.onload = function() {
  videoMethods.renderVideoPlayButton();
  if(!!sliderButtons) {
	let modal = document.getElementById('modal');
	let video = [...document.querySelectorAll('.modal video')];
	let currentVideo;
	sliderButtons.forEach((button) => {
		let modalClose = modal.querySelector('.modal-close');
		button.addEventListener('click', function() {
			currentVideo = video.find( vidID => vidID.id == this.dataset.modalId);
		  	modal.classList.add('active');
			body.classList.add('modal-active');
			currentVideo.play();
		});
		modalClose.addEventListener('click', ()=> {
			currentVideo.pause();
			modal.classList.remove('active');
			body.classList.remove('modal-active');
		});
	});
  }
};