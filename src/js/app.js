import {jQuery} from 'jquery';

(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='TICKETYPE';ftypes[3]='text';fnames[5]='COMPANY';ftypes[5]='text';}(jQuery));

let menuButton = document.getElementById('mobile-menu-button');
let mainMenu = document.getElementById('main-menu');

menuButton.addEventListener('click', function() {
    mainMenu.classList.toggle('active');
});