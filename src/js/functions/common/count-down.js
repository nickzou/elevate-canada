const countDown = (targetDate, daysElement = 0, hoursElement = 0, minutesElement = 0, secondsElement = 0) => {
    let countdownDate = new Date(targetDate).getTime();
        
    let x = setInterval(()=> {

        let now = new Date().getTime();
        let distance = countdownDate - now;
        let days = Math.floor(distance / (1000 * 60 * 60 * 24));
        let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (!!daysElement) {
            daysElement.innerHTML = days;
        }
        if (!!hoursElement) {
            hoursElement.innerHTML = hours;
        }
        if (!!minutesElement) {
            minutesElement.innerHTML = minutes;
        }
        if (!!secondsElement) {
            secondsElement.innerHTML = seconds;
        }

    }, 1000);
}

export default countDown;