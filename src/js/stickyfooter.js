import countDown from './functions/common/count-down';

let isStickyFooter = true;
let footer = document.getElementById('site-footer');
let footerContainer = document.querySelector('.footer-container');
let stickyFooter = document.getElementById('sticky-footer');
let stickyClose = document.getElementById('sticky-footer-close-button');
let targetTime = footer.dataset.countdownValue;
let daysEl = document.querySelector('.sticky-footer-time-wrapper #days');
let hoursEl = document.querySelector('.sticky-footer-time-wrapper #hours');
let minutesEl = document.querySelector('.sticky-footer-time-wrapper #minutes');
let secondsEl = document.querySelector('.sticky-footer-time-wrapper #seconds');

if (localStorage.getItem('isStickyFooter') == null ) {
    localStorage.setItem('isStickyFooter', JSON.stringify(isStickyFooter));
    stickyFooter.classList.add('enabled');
    footerContainer.classList.add('sticky-footer-enabled');
    countDown(targetTime, daysEl, hoursEl, minutesEl, secondsEl);
} else {
    isStickyFooter = localStorage.getItem('isStickyFooter');
    if(isStickyFooter == 'true') {
        stickyFooter.classList.add('enabled');
        footerContainer.classList.add('sticky-footer-enabled');
        countDown(targetTime, daysEl, hoursEl, minutesEl, secondsEl);
    } else if (isStickyFooter == 'false') {
        stickyFooter.classList.remove('enabled');
        footerContainer.classList.remove('sticky-footer-enabled');
    }
}

stickyClose.addEventListener('click', ()=> {
    isStickyFooter = false;
    localStorage.setItem('isStickyFooter', JSON.stringify(isStickyFooter));
    stickyFooter.classList.remove('enabled');
    footerContainer.classList.remove('sticky-footer-enabled');
});

