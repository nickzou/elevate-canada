import Swiper from 'swiper';

//inits festival overview slider
let eventsSlider = new Swiper('#events', {
    direction: 'horizontal',
    loop: true,
  
    slidesPerView: 4,
    spaceBetween: 15,
    centeredSlides: true,
  
    navigation: {
      nextEl: '#events-slider-next',
      prevEl: '#events-slider-prev',
    },
  
    breakpoints: {
      1399: {
        slidesPerView: 3
      },
      1199: {
        slidesPerView: 2
      },
      767: {
        slidesPerView: 1
      }
    }
  });