<?php
    /*
        Template Name: Michelle Obama
        Template Post Type: events
    */

    get_header();

    get_template_part('template-parts/events/single/michelle-obama/hero');
    get_template_part('template-parts/events/single/details');
    get_template_part('template-parts/events/single/michelle-obama/block_quote');
    get_template_part('template-parts/events/single/michelle-obama/tickets');
    get_template_part('template-parts/events/single/michelle-obama/profile');

    get_footer();

?>