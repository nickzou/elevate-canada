<?php
    /*
        Template Name: Page Extended
        Template Post Type: page
    */

    get_header();

    get_template_part('template-parts/common/hero');
	get_template_part('template-parts/page-extended/main-content');
	get_template_part('template-parts/page-extended/flexible-content');

    get_footer();

?>