<?php
    /*
        Template Name: Tickets Page
        Template Post Type: page
    */

    get_header();

	get_template_part('template-parts/tickets/countdown');
	get_template_part('template-parts/tickets/purchase');

?>

<script>
	TitoDevelopmentMode = true;
</script>

<?php

    get_footer();

?>