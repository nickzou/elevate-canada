<?php

	get_header();

	get_template_part('template-parts/front-page/hero');
	get_template_part('template-parts/front-page/main-content');
	get_template_part('template-parts/front-page/gallery');
	get_template_part('template-parts/front-page/speakers');
	get_template_part('template-parts/common/events');
	get_template_part('template-parts/front-page/video');
	get_template_part('template-parts/front-page/second-content');
	get_template_part('template-parts/front-page/partners');
	get_template_part('template-parts/front-page/modal');

	get_footer();

?>