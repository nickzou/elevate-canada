    <footer id="site-footer" class="site-footer" <?php if(get_field('countdown_timer', 'options')['disable']==false): ?>data-countdown-value='<?php echo get_field('countdown_timer', 'options')['timer']; ?>'<?php endif; ?>>
        <?php get_template_part('template-parts/footer/pre-footer'); ?>
        <?php get_template_part('template-parts/footer/main-footer'); ?>
    </footer>
    <?php
        get_template_part('template-parts/footer/sticky-footer');
    ?>
</body>
<?php wp_footer();?>