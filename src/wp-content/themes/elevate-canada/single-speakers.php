<?php

  get_header();

  get_template_part('template-parts/speakers/single/main-content');
  get_template_part('template-parts/speakers/single/recommended-speakers');

  get_footer();

?>