<div class="row details-row">
    <div class="col col-12 col-md-4">
        <span class="section-subtitle"><?php echo $component_args['subtitle']; ?></span>
        <h1 class="section-title"><?php echo $component_args['title'] ?></h1>
        <h2 class="section-details-text"><?php echo $component_args['venue']; ?></h2>
        <h3 class="section-details-text"><?php echo $component_args['address']; ?></h3>
    </div>
    <div class="col col-12 col-md-8">
        <p><?php echo $component_args['content']; ?></p>
    </div>
</div>