<a class="speaker-tile" href="<?php echo $component_args['url']; ?>">
    <div class="speaker-logo-wrapper">
        <img class="speaker-logo" src="<?php echo $component_args['logo']; ?>">
    </div>
    <div class="speaker-headshot-wrapper">
        <img class="speaker-headshot" src="<?php echo $component_args['image']; ?>">
    </div>
    <h2 class="speaker-name"><?php echo $component_args['name']; ?></h2>
<h3 class="speaker-title"><?php echo $component_args['title']; ?><?php if(!!$component_args['organization']): ?> of <?php echo $component_args['organization']; endif; ?></h3>
</a>