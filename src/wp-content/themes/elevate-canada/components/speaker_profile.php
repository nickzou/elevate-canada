<div class="row speaker-profile-row">
    <div class="col col-12 col-md-5">
        <?php if(!!$component_args['logo']): ?>
            <div class="speaker-logo-wrapper">
                <img class="" src="<?php echo $component_args['logo']; ?>" />
            </div>
        <?php endif;?>
        <div class="speaker-image-wrapper">
            <img src="<?php echo $component_args['image'] ?>" />
        </div>
    </div>
    <div class="col col-12 col-md-7">
        <h1 class="speaker-title"><?php echo $component_args['title']; ?></h1>
        <h2 class="speaker-subtitle"><?php echo $component_args['subtitle']; ?></h2>
        <p><?php echo $component_args['content']; ?></p>
        <div class="social-media-wrapper">
            <?php if(!!$component_args['facebook']): ?>
                <a class="social-media-link" href="<?php echo $component_args['facebook']; ?>" target="_blank">
                    <img class="social-media facebook" src="<?php echo get_template_directory_uri(); ?>/assets/Facebook.png">
                </a>
            <?php endif; ?>
            <?php if(!!$component_args['twitter']): ?>
                <a class="social-media-link" href="<?php echo $component_args['twitter']; ?>" target="_blank">
                    <img class="social-media twitter" src="<?php echo get_template_directory_uri(); ?>/assets/Twitter.png">
                </a>
            <?php endif; ?>
            <?php if(!!$component_args['instagram']): ?>
                <a class="social-media-link" href="<?php echo $component_args['instagram']; ?>" target="_blank">
                    <img class="social-media instagram" src="<?php echo get_template_directory_uri(); ?>/assets/Instagram.png">
                </a>
            <?php endif; ?>
            <?php if(!!$component_args['linkedin']): ?>
                <a class="social-media-link" href="<?php echo $component_args['linkedin']; ?>" target="_blank">
                    <img class="social-media linkedin" src="<?php echo get_template_directory_uri(); ?>/assets/LinkedIn.png">
                </a>
            <?php endif; ?>
            <?php if(!!$component_args['website']): ?>
                <a class="social-media-link" href="<?php echo $component_args['website']; ?>" target="_blank">
                    <img class="social-media website" src="<?php echo get_template_directory_uri(); ?>/assets/Website.png">
                </a>
            <?php endif; ?>
            <?php if(!!get_field('social_media_links')['blog']): ?>
                <a class="social-media-link" href="<?php echo get_field('social_media_links')['blog']; ?>" target="_blank">
                    <img class="social-media blog" src="<?php echo get_template_directory_uri(); ?>/assets/RSS.png">
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>