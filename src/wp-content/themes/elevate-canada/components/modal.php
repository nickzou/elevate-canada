<div id="modal" class="modal">
    <div class="lightbox-container">
        <?php
            if(!!$component_args['video']):
        ?>
            <div id="modal-<?php echo $component_args['id']; ?>-video-wrapper" class="video-wrapper">
                <video id="<?php echo $component_args['id']; ?>" poster="" controls>
                    <source src="<?php echo $component_args['url']; ?>" type="video/mp4">
                </video>
            </div>
        <?php
            else:
            endif;
        ?>
        <button class="modal-close"><ion-icon name="close"></ion-icon></button>
    </div>
</div>