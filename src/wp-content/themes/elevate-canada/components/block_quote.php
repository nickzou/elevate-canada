<section class="container-fluid section block-quote-container">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col col-12">
                    <h2 class="block-quote-title"><?php echo $component_args['title']; ?></h2>
                </div>
            </div>
        </div>
    </div>
</section>