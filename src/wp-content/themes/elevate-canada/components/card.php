<div class="card">
    <div class="card-title-wrapper">
        <h2 class="card-title"><?php echo $component_args['title']; ?></h2>
        <?php
            if(!!$component_args['subtitle']):
        ?>
            <p class="card-subtitle"><?php echo $component_args['subtitle']; ?></p>
        <?php
            endif;
        ?>
    </div>
    <div class="card-body">
        <?php echo $component_args['card_body']; ?>
    </div>
    <?php
        if(!!$component_args['custom_button']):
            echo $component_args['custom_button_template'];
        else:
    ?>
        <div class="card-button-wrapper">
            <a class="button hard full-width red" href="<?php echo $component_args['button_url'] ?>"><?php echo $component_args['button_text']; ?></a>
        </div>
    <?php endif;?>
</div>