<?php

    require get_template_directory() . '/functions/global/register_global_resources.php';
    require get_template_directory() . '/functions/global/enqueue_global_resources.php';
    require get_template_directory() . '/functions/global/my_acf_init.php';
    require get_template_directory() . '/functions/global/acf_local_json_save.php';
    require get_template_directory() . '/functions/global/acf_local_json_load.php';
    require get_template_directory() . '/functions/global/theme_setup.php';
    require get_template_directory() . '/functions/global/theme_options.php';
    require get_template_directory() . '/functions/global/custom_excerpt_more.php';
    require get_template_directory() . '/functions/global/get_component.php';
    require get_template_directory() . '/functions/custom-post-types/tickets/tickets_custom_post_type.php';
    require get_template_directory() . '/functions/custom-post-types/tickets/tickets_options.php';
    require get_template_directory() . '/functions/custom-post-types/speakers/speakers_custom_post_type.php';
    require get_template_directory() . '/functions/custom-post-types/employees/employees_custom_post_type.php';
    require get_template_directory() . '/functions/custom-post-types/speakers/speakers_options.php';
    require get_template_directory() . '/functions/custom-post-types/events/events_custom_post_type.php';
    require get_template_directory() . '/functions/custom-post-types/events/events_custom_main_loop.php';

?>