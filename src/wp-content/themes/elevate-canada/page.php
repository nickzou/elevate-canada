<?php

    get_header();

    if(have_posts()):
        while(have_posts()): the_post();

        get_template_part('template-parts/common/hero');
        get_template_part('template-parts/page/main-content');

        endwhile;

    else:

    echo '<p>No content found</p>';

    endif;

    get_footer();

?>