<section id="mc_embed_signup" class="container-fluid pre-footer-container">
    <div class="row">
        <form action="https://elevatetoronto.us16.list-manage.com/subscribe/post?u=ff9df5210f28180a1a2524ee9&id=865e316dbf" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="container validate" target="_blank" novalidate>
            <div class="row">
                <div class="col col-12 text-center">
                    <h1 class="footer-heading">Don't Want to Miss a Thing?</h1>
                    <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                        <div class="mc-field-group">
                            <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
                        </label>
                            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                        </div>
                        <div class="mc-field-group">
                            <label for="mce-FNAME">First Name  <span class="asterisk">*</span>
                        </label>
                            <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ff9df5210f28180a1a2524ee9_865e316dbf" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="connect" name="subscribe" id="mc-embedded-subscribe" class="button red"></div>
                    </div>  
                </div>  
            </div>
        </form>
    </div>
</section>
<script type='text/javascript'></script>