<div id="sticky-footer" class="sticky-footer-wrapper">
    <div class="container">
        <?php
            $sticky_footer = get_field('sticky_footer', 'options');
        ?>
        <div class="row sticky-footer-row">
            <div class="col col-12 col-md-5 d-flex justify-content-center justify-content-md-start align-items-center">
                <p><?php echo $sticky_footer['footer_text']; ?></p>
            </div>
            <div class="col col-12 col-md-7 d-flex justify-content-around align-items-center">
                <div class="sticky-footer-time-wrapper">
                    <h3 class="sticky-footer-heading">days</h3>
                    <div id="days" class="sticky-footer-time-unit">
                    </div>
                </div>
                <div class="sticky-footer-time-wrapper">
                    <h3 class="sticky-footer-heading">hours</h3>
                    <div id="hours" class="sticky-footer-time-unit">
                    </div>
                </div>
                <div class="sticky-footer-time-wrapper">
                    <h3 class="sticky-footer-heading">mins</h3>
                    <div id="minutes" class="sticky-footer-time-unit">
                    </div>
                </div>
                <div class="sticky-footer-time-wrapper">
                    <h3 class="sticky-footer-heading">secs</h3>
                    <div id="seconds" class="sticky-footer-time-unit">
                    </div>
                </div>
                <a href="<?php echo $sticky_footer['link']['url']; ?>" class="button white"><?php echo $sticky_footer['text']; ?></a>
            </div>
            <button id="sticky-footer-close-button" class="sticky-footer-close-button">
                <ion-icon name="close"></ion-icon>
            </button>
        </div>
    </div>
</div>