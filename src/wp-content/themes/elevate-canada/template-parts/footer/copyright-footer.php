<div class="container copyright-footer-container">
    <div class="row">
        <div class="col col-12">
            <p class="copyright">Copyright <?php echo date('Y'); ?> - Elevate</p>
        </div>
    </div>
</div>