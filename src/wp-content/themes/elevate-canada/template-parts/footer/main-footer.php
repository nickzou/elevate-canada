<section class="container-fluid footer-container">
    <div class="row">
        <div class="container footer-inner-container">
            <div class="row">
                <div class="col col-12 col-md-6 col-lg-3 footer-menu-col">
                    <div class="footer-menu-wrapper">
                        <h2 class="footer-menu-heading">Featured Events</h2>
                        <?php
                            $footer_1_menu_args = array(
                                'theme_location' => 'footer-1'
                            );
                            wp_nav_menu($footer_1_menu_args);
                        ?>
                    </div>
                </div>
                <div class="col col-12 col-md-6 col-lg-3 footer-menu-col">
                    <div class="footer-menu-wrapper">
                        <h2 class="footer-menu-heading">Festival Stages</h2>
                        <?php
                            $footer_2_menu_args = array(
                                'theme_location' => 'footer-2'
                            );
                            wp_nav_menu($footer_2_menu_args);
                        ?>
                    </div>
                </div>
                <div class="col col-12 col-md-6 col-lg-3 footer-menu-col">
                    <div class="footer-menu-wrapper">
                        <h2 class="footer-menu-heading">Information</h2>
                        <?php
                            $footer_3_menu_args = array(
                                'theme_location' => 'footer-3'
                            );
                            wp_nav_menu($footer_3_menu_args);
                        ?>
                    </div>
                </div>
                <div class="col col-12 col-md-6 col-lg-3 footer-menu-col">
                    <!-- <div class="footer-menu-wrapper">
                        <h2 class="footer-menu-heading">Download App</h2>
                        <?php
                            $footer_4_menu_args = array(
                                'theme_location' => 'footer-4'
                            );
                            wp_nav_menu($footer_4_menu_args);
                        ?>
                    </div> -->
                    <div class="footer-menu-wrapper">
                        <h2 class="footer-menu-heading">Social</h2>
                        <?php $social_media_links = get_field('social_media_links', 'options'); ?>
                        <div class="footer-menu-social-wrapper">
                            <?php
                                if(!!$social_media_links['facebook']):
                            ?>
                                <a href="<?php echo $social_media_links['facebook']; ?>" class="footer-menu-social" target="_blank">
                                    <ion-icon name="logo-facebook"></ion-icon>
                                </a>
                            <?php
                                endif;
                            ?>
                            <?php
                                if(!!$social_media_links['twitter']):
                            ?>
                                <a href="<?php echo $social_media_links['twitter']; ?>" class="footer-menu-social" target="_blank">
                                    <ion-icon name="logo-twitter"></ion-icon>
                                </a>
                            <?php
                                endif;
                            ?>
                            <?php
                                if(!!$social_media_links['instagram']):
                            ?>
                                <a href="<?php echo $social_media_links['instagram']; ?>" class="footer-menu-social" target="_blank">
                                    <ion-icon name="logo-instagram"></ion-icon>
                                </a>
                            <?php
                                endif;
                            ?>
                            <?php
                                if(!!$social_media_links['youtube']):
                            ?>
                                <a href="<?php echo $social_media_links['youtube']; ?>" class="footer-menu-social" target="_blank">
                                    <ion-icon name="logo-youtube"></ion-icon>
                                </a>
                            <?php
                                endif;
                            ?>
                            <?php
                                if(!!$social_media_links['linkedin']):
                            ?>
                                <a href="<?php echo $social_media_links['linkedin']; ?>" class="footer-menu-social" target="_blank">
                                    <ion-icon name="logo-linkedin"></ion-icon>
                                </a>
                            <?php
                                endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_template_part('template-parts/footer/copyright-footer'); ?>
    </div>
</section>