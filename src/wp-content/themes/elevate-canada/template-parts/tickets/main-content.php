<section class="container-fluid section hero-container">
	<div class="row">
        <div class="container">
            <div class="row">
                <?php

                    if (have_posts()) :

                    while (have_posts()) : the_post(); ?>
                    <div class="col col-12">
                        <?php the_content(); ?>
                    </div>
                    <?php endwhile;

                    else:

                    echo '<p>No content found</p>';

                    endif;

                ?>
            </div>
        </div>
	</div>
</section>