<section class="container-fluid section hero-container">
	<div class="row">
        <div class="col col-12 text-center">
            <h2 class="section-subtitle">Ticket prices go up in<br/> <span id="days"></span> days, <span id="hours"></span> hours and <span id="minutes"></span> mins.<br/> Don't miss out!</h2>
        </div>
	</div>
</section>