<?php
	$earlybird = get_field('earlybird_end_date', 'options');
	$midsummer = get_field('midsummer_end_date', 'options');
	$regular = get_field('regular_end_date', 'options');
	$walkup = get_field('event_date', 'options');

	$date_earlybird = new DateTime($earlybird);
	$date_midsummer = new DateTime($midsummer);
	$date_regular = new DateTime($regular);
	$date_walkup = new DateTime($walkup);

	$date_earlybird_filtered = $date_earlybird->format('M d');
	$date_midsummer_filtered = $date_midsummer->format('M d');
	$date_regular_filtered = $date_regular->format('M d');
	$date_walkup_filtered = $date_walkup->format('M d');
?>
<section class="container-fluid section tickets-purchase-header-container">
	<div class="row">
		<div class="col col-12">
			<h1 class="section-title">Festival Passes</h1>
		</div>
	</div>
</section>
<section class="container-fluid tickets-purchase-tab-container">
	<div class="row">
		<div class="col col-3 p-0">
			<button id="earlybird-tab" class="ticket-purchase-tabs single" data-ticket-type="earlybird" data-countdown-value="<?php echo $earlybird; ?>">
				<h2 class="tab-title">Early Bird</h2>
				<h3 class="tab-date">Ends <?php echo $date_earlybird_filtered; ?></h3>
			</button>
		</div>
		<div class="col col-3 p-0">
			<button id="midsummer-tab" class="ticket-purchase-tabs single" data-ticket-type="midsummer" data-countdown-value="<?php echo $midsummer; ?>">
				<h2 class="tab-title">Mid Summer</h2>
				<h3 class="tab-date">Ends <?php echo $date_midsummer_filtered; ?></h3>
			</button>
		</div>
		<div class="col col-3 p-0">
			<button id="regular-tab" class="ticket-purchase-tabs single" data-ticket-type="regular" data-countdown-value="<?php echo $regular; ?>">
				<h2 class="tab-title">Regular</h2>
				<h3 class="tab-date">Ends <?php echo $date_regular_filtered; ?></h3>
			</button>
		</div>
		<div class="col col-3 p-0">
			<button id="walkup-tab" class="ticket-purchase-tabs single" data-ticket-type="walkup" data-countdown-value="<?php echo $walkup; ?>">
				<h2 class="tab-title">Walk Up</h2>
				<h3 class="tab-date">On <?php echo $date_walkup_filtered; ?></h3>
			</button>
		</div>
	</div>
</section>
<section class="container-fluid section tickets-purchase-container">
	<div class="row">
		<div class="col col-12 p-0">
			<div id="slider" class="swiper-container festival-purchase-slider">
				<div class="swiper-wrapper">
					<?php

						function render_ticket_includes($includes) {
							$text ='';
							foreach ($includes as $include) {
								if (!!$include['included']) {
									$text .= '
									<div class="row">
										<div class="col col-12">
											<div class="included-features">
												<div class="icon">
													<ion-icon name="checkmark-circle"></ion-icon>
												</div>
												<div class="text">' .
													$include['text']	
												. '</div>
											</div>
										</div>
									</div>';
								} else {
									$text .= '
									<div class="row">
										<div class="col col-12">
											<div class="included-features unavailable">
												<div class="icon">
													<ion-icon name="close-circle"></ion-icon>
												</div>
												<div class="text">' .
													$include['text']	
												. '</div>
											</div>
										</div>
									</div>';
								}
							}
							return $text;
						};

						$args = array(
							'post_type' => 'tickets',
							'orderby' => 'menu_order',
							'order' => 'ASC'
						);
						$tickets = new WP_Query($args);
						if($tickets->have_posts()):
							while($tickets->have_posts()): $tickets->the_post();

							$prices = get_field('prices');
							$includes = get_field('includes');

							$features = render_ticket_includes($includes);

							$card_body_args = array(
								'{date_earlybird}' => $date_earlybird->format('M d'),
								'{date_midsummer}' => $date_midsummer->format('M d'),
								'{date_regular}' => $date_regular->format('M d'),
								'{date_walkup}' => $date_walkup->format('M d'),
								'{price_earlybird}' => $prices['earlybird'],
								'{earlybird_hst}' => number_format((int) $prices['earlybird'] * .13, 2),
								'{price_midsummer}' => $prices['midsummer'],
								'{midsummer_hst}' => number_format((int) $prices['midsummer'] * .13, 2),
								'{price_regular}' => $prices['regular'],
								'{regular_hst}' => number_format((int) $prices['regular'] * .13, 2),
								'{price_walkup}' => $prices['walkup'],
								'{walkup_hst}' => number_format((int) $prices['walkup'] * .13, 2),
								'{features}' => $features
							);
							
							$card_body_template =
							'<div class="container tickets-card-info-container">
								<div class="row included-row">
									<div class="col col-12">
										<div class="row">
											<div class="col col-12">
												<h2 class="includes-title">What\'s included</h2>
											</div>
										</div>
										{features}
									</div>
								</div>
								<div class="row prices-row earlybird single" data-time-frame="earlybird">
									<div class="col col-6 price-col current-price-col current">
										<h2 class="ticket-subtitle">Until {date_earlybird}</h2>
										<h3 class="ticket-price">${price_earlybird}</h3>
										<h4 class="ticket-tax">Plus ${earlybird_hst} HST</h4>
									</div>
									<div class="col col-6 price-col future-price-col">
										<h2 class="ticket-subtitle">Walk Up</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
								<div class="row prices-row midsummer single" data-time-frame="midsummer">
									<div class="col col-6 price-col current-price-col current">
										<h2 class="ticket-subtitle">Until {date_midsummer}</h2>
										<h3 class="ticket-price">${price_midsummer}</h3>
										<h4 class="ticket-tax">Plus ${midsummer_hst} HST</h4>
									</div>
									<div class="col col-6 price-col future-price-col">
										<h2 class="ticket-subtitle">Walk Up</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
								<div class="row prices-row regular single" data-time-frame="regular">
									<div class="col col-6 price-col current-price-col current">
										<h2 class="ticket-subtitle">Until {date_regular}</h2>
										<h3 class="ticket-price">${price_regular}</h3>
										<h4 class="ticket-tax">Plus ${regular_hst} HST</h4>
									</div>
									<div class="col col-6 price-col future-price-col">
										<h2 class="ticket-subtitle">Walk Up</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
								<div class="row prices-row walkup single" data-time-frame="walkup">
									<div class="col col-6 price-col current-price-col no-slash current">
										<h2 class="ticket-subtitle">Until {date_walkup}</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
							</div>';

						?>

						<div class="swiper-slide">

						<?php

							$tito = get_field('tito');						

							$tickets_args = array(
								'title' => get_the_title(),
								'subtitle' => get_the_excerpt(),
								'card_body' => strtr($card_body_template, $card_body_args),
								'custom_button' => 1,
								'custom_button_template' => '
									<tito-button event="' . $tito['event'] . '" releases="'. $tito['releases']['single'] .'" ssl-check-disabled>
										' . $tito['button_text'] . '
									</tito-button>
								'
							);

							get_component('components/card', $tickets_args);

						?>

						</div>

						<?php

							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="container-fluid section tickets-purchase-header-container">
	<div class="row">
		<div class="col col-12">
			<h1 class="section-title">Group Tickets</h1>
		</div>
	</div>
</section>
<section class="container-fluid tickets-purchase-tab-container">
	<div class="row">
		<div class="col col-3 p-0">
			<button id="group-earlybird-tab" class="ticket-purchase-tabs group"  data-ticket-type="earlybird">
				<h2 class="tab-title">Early Bird</h2>
				<h3 class="tab-date">Ends <?php echo $date_earlybird_filtered; ?></h3>
			</button>
		</div>
		<div class="col col-3 p-0">
			<button id="group-midsummer-tab" class="ticket-purchase-tabs group"  data-ticket-type="midsummer">
				<h2 class="tab-title">Mid Summer</h2>
				<h3 class="tab-date">Ends <?php echo $date_midsummer_filtered; ?></h3>
			</button>
		</div>
		<div class="col col-3 p-0">
			<button id="group-regular-tab" class="ticket-purchase-tabs group" data-ticket-type="regular">
				<h2 class="tab-title">Regular</h2>
				<h3 class="tab-date">Ends <?php echo $date_regular_filtered; ?></h3>
			</button>
		</div>
		<div class="col col-3 p-0">
			<button id="group-walkup-tab" class="ticket-purchase-tabs group" data-ticket-type="walkup">
				<h2 class="tab-title">Walk Up</h2>
				<h3 class="tab-date">On <?php echo $date_walkup_filtered; ?></h3>
			</button>
		</div>
	</div>
</section>
<section class="container-fluid section tickets-purchase-container">
	<div class="row">
		<div class="col col-12 p-0">
			<div id="group-slider" class="swiper-container festival-purchase-slider">
				<div class="swiper-wrapper">
					<?php
						$args = array(
							'post_type' => 'tickets',
							'orderby' => 'menu_order',
							'order' => 'ASC'
						);
						$tickets = new WP_Query($args);
						if($tickets->have_posts()):
							while($tickets->have_posts()): $tickets->the_post();

							$prices = get_field('prices');
							$includes = get_field('includes');

							$features = render_ticket_includes($includes);

							$card_body_args = array(
								'{date_earlybird}' => $date_earlybird->format('M d'),
								'{date_midsummer}' => $date_midsummer->format('M d'),
								'{date_regular}' => $date_regular->format('M d'),
								'{date_walkup}' => $date_walkup->format('M d'),
								'{price_earlybird}' => (int) $prices['earlybird'] * .8,
								'{earlybird_hst}' => number_format((int) $prices['earlybird'] * .8 * .13, 2),
								'{price_midsummer}' => (int) $prices['midsummer'] * .8,
								'{midsummer_hst}' => number_format((int) $prices['midsummer'] * .8 * .13, 2),
								'{price_regular}' => (int) $prices['regular'] * .8,
								'{regular_hst}' => number_format((int) $prices['regular'] * .8 * .13, 2),
								'{price_walkup}' => (int) $prices['walkup'] * .8,
								'{walkup_hst}' => number_format((int) $prices['walkup'] * .8 * .13, 2)
							);
							
							$card_body_template =
							'<div class="container tickets-card-info-container">
								<div class="row included-row">
									<div class="col col-12">
										<div class="row">
											<div class="col col-12">
												<h2 class="includes-title">5 pack/price per ticket</h2>
											</div>
										</div>
									</div>
								</div>
								<div class="row prices-row earlybird group" data-time-frame="earlybird">
									<div class="col col-6 price-col current-price-col current">
										<h2 class="ticket-subtitle">Until {date_earlybird}</h2>
										<h3 class="ticket-price">${price_earlybird}</h3>
										<h4 class="ticket-tax">Plus ${earlybird_hst} HST</h4>
									</div>
									<div class="col col-6 price-col future-price-col">
										<h2 class="ticket-subtitle">Walk Up</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
								<div class="row prices-row midsummer group" data-time-frame="midsummer">
									<div class="col col-6 price-col current-price-col current">
										<h2 class="ticket-subtitle">Until {date_midsummer}</h2>
										<h3 class="ticket-price">${price_midsummer}</h3>
										<h4 class="ticket-tax">Plus ${midsummer_hst} HST</h4>
									</div>
									<div class="col col-6 price-col future-price-col">
										<h2 class="ticket-subtitle">Walk Up</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
								<div class="row prices-row regular group" data-time-frame="regular">
									<div class="col col-6 price-col current-price-col current">
										<h2 class="ticket-subtitle">Until {date_regular}</h2>
										<h3 class="ticket-price">${price_regular}</h3>
										<h4 class="ticket-tax">Plus ${regular_hst} HST</h4>
									</div>
									<div class="col col-6 price-col future-price-col">
										<h2 class="ticket-subtitle">Walk Up</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
								<div class="row prices-row walkup group" data-time-frame="walkup">
									<div class="col col-6 price-col current-price-col no-slash current">
										<h2 class="ticket-subtitle">Until {date_walkup}</h2>
										<h3 class="ticket-price">${price_walkup}</h3>
										<h4 class="ticket-tax">Plus ${walkup_hst} HST</h4>
									</div>
								</div>
							</div>';

						?>

						<div class="swiper-slide">

						<?php

							$tito = get_field('tito');							

							$tickets_args = array(
								'title' => get_the_title(),
								'subtitle' => get_the_excerpt(),
								'card_body' => strtr($card_body_template, $card_body_args),
								'custom_button' => 1,
								'custom_button_template' => '
									<tito-button event="' . $tito['event'] . '" releases="'. $tito['releases']['group'] .'" ssl-check-disabled>
										' . $tito['button_text'] . '
									</tito-button>
								'
							);

							get_component('components/card', $tickets_args);

						?>

						</div>

						<?php

							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
	</div>
</section>