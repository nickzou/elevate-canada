<section class="container section speaker-profile-section">
  <div class="row">

        <?php

          if (have_posts()) :

            while (have_posts()) : the_post(); ?>
            <div class="col col-12 col-md-5">
                <?php if(!!get_field('logo')): ?>
                    <div class="speaker-logo-wrapper">
                        <img class="" src="<?php echo get_field('logo')['sizes']['medium']; ?>" />
                    </div>
                <?php endif; ?>
                <div class="speaker-image-wrapper">
                    <?php the_post_thumbnail('medium_large'); ?>
                </div>
            </div>
            <div class="col col-12 col-md-7">
                <h1 class="speaker-title"><?php the_title(); ?></h1>
                <h2 class="speaker-subtitle"><?php the_field('subtitle'); ?></h2>
                <p><?php the_content(); ?></p>
                <div class="social-media-wrapper">
                    <?php if(!!get_field('social_media_links')['facebook']): ?>
                        <a class="social-media-link" href="<?php echo get_field('social_media_links')['facebook']; ?>" target="_blank">
                            <img class="social-media facebook" src="<?php echo get_template_directory_uri(); ?>/assets/Facebook.png">
                        </a>
                    <?php endif; ?>
                    <?php if(!!get_field('social_media_links')['twitter']): ?>
                        <a class="social-media-link" href="<?php echo get_field('social_media_links')['twitter']; ?>" target="_blank">
                            <img class="social-media twitter" src="<?php echo get_template_directory_uri(); ?>/assets/Twitter.png">
                        </a>
                    <?php endif; ?>
                    <?php if(!!get_field('social_media_links')['instagram']): ?>
                        <a class="social-media-link" href="<?php echo get_field('social_media_links')['instagram']; ?>" target="_blank">
                            <img class="social-media instagram" src="<?php echo get_template_directory_uri(); ?>/assets/Instagram.png">
                        </a>
                    <?php endif; ?>
                    <?php if(!!get_field('social_media_links')['linkedin']): ?>
                        <a class="social-media-link" href="<?php echo get_field('social_media_links')['linkedin']; ?>" target="_blank">
                            <img class="social-media linkedin" src="<?php echo get_template_directory_uri(); ?>/assets/LinkedIn.png">
                        </a>
                    <?php endif; ?>
                    <?php if(!!get_field('social_media_links')['website']): ?>
                        <a class="social-media-link" href="<?php echo get_field('social_media_links')['website']; ?>" target="_blank">
                            <img class="social-media website" src="<?php echo get_template_directory_uri(); ?>/assets/Website.png">
                        </a>
                    <?php endif; ?>
                    <?php if(!!get_field('social_media_links')['blog']): ?>
                        <a class="social-media-link" href="<?php echo get_field('social_media_links')['blog']; ?>" target="_blank">
                            <img class="social-media blog" src="<?php echo get_template_directory_uri(); ?>/assets/RSS.png">
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <?php endwhile;

            else:

            echo '<p>No content found</p>';

          endif;

        ?>

  </div>
</section>