<section class="container-fluid section related-speakers-container">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col col-12">
                    <h2 class="recommended-speakers-title">Recommended Speakers</h2>
                </div>
            </div>
            <div class="row">
                <?php $featured_speakers = get_field('featured_speakers', 'options');

                    foreach ($featured_speakers as $featured_speaker):

                        $speaker = $featured_speaker['speaker'];

                        $speaker_args = array(
                            'url' => get_the_permalink($speaker->ID),
                            'logo' => get_field('logo', $speaker->ID)['sizes']['large'],
                            'image' => get_the_post_thumbnail_url($speaker->ID, 'medium'),
                            'name' => $speaker->post_title,
                            'title' => get_field('subtitle', $speaker->ID),
                            'organization' => get_field('organization', $speaker->ID)
                        );
                ?>
                <div class="col col-6 col-lg-fifth">       
                    <?php get_component('components/speaker_tile', $speaker_args); ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>