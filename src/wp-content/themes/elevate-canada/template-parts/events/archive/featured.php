<section class="container section main-content-container">
	<div class="row">
		<div class="col col-12">
			<h1 class="section-heading">Featured Events</h1>
		</div>
	</div>
  <div class="row">
  	<div class="col col-12">
        <?php

          if (have_posts()) :

            while (have_posts()) : the_post(); ?>
            
				<div class="row featured-event-row">
					<div class="col col-12 col-md-6 col-lg-4">
						<div class="featured-image-wrapper">
							<a class="featured-image" href="<?php echo get_the_permalink($speaker->ID); ?>">
								<?php the_post_thumbnail('medium'); ?>
		  					</a>
							<?php $speakers = get_field('speakers'); ?>
							<div class="row speakers-row">
								<?php
									if(!!$speakers):
										foreach($speakers as $speaker):
								?>
								<div class="col col-4">
									<a class="speaker-avatar" href="<?php echo get_the_permalink($speaker->ID); ?>">
										<?php echo get_the_post_thumbnail($speaker->ID, 'medium'); ?>
									</a>
								</div>
								<?php
										endforeach;
									endif;
								?>
							</div>
						</div>
					</div>	
					<div class="col col-12 col-md-6 col-lg-8">
						<h2 class="featured-event-title"><?php echo get_the_title(); ?></h2>
						<h3 class="featured-event-venue"><?php echo get_field('venue'); ?></h3>
						<h4 class="featured-event-date"><?php echo get_field('dates')['start_date'] . " - " . get_field('dates')['end_date']; ?></h4>
						<p><?php echo get_the_excerpt(); ?></p>
						<a class="button red" href="<?php echo get_the_permalink(); ?>">learn more</a>
					</div>
					
		  		</div>
            <?php endwhile;

            else:

            echo '<p>No content found</p>';

          endif;

		?>
	</div>
  </div>
</section>