<section class="container-fluid section hero-section-container">
    <div class="row hero-content-row">
        <div class="container">
            <div class="row">
                <div class="col col-12 hero-info-col">
                    <div class="hero-info-wrapper">
                        <h1 class="hero-heading"><?php the_title(); ?></h1>
                        <h2 class="hero-subheading"><?php echo get_field('tagline'); ?></h2>
                        <?php
                            $dates = get_field('dates');
                            $date = new DateTime($dates['start_date']);
                        ?>
                        <h3 class="hero-dates"><?php echo $date->format('D'); ?>,
                            <?php echo $date->format('M j'); ?> @ <?php echo $date->format('gA') ?>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-image-wrapper">
        
    </div>
</section>