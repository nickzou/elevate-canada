<section class="container section details-section-container">
    <div class="row">
        <div class="col col-12">
            <h1 class="section-heading">Event Details</h1>
        </div>
    </div>
    <?php

        $dates = get_field('dates');
        $date = new DateTime($dates['start_date']);
        $doors_open = $dates['doors_open'];
    
        $details_args = array(
            'subtitle' => $date->format('l, M j') . '@' . $date->format('gA'),
            'title' => 'Elevate presents <br/>' . get_the_title(),
            'venue' => get_field('venue'),
            'address' => get_field('address'),
            'content' => get_the_content(),
        );

        get_component('components/details', $details_args);
    
    ?>
</section>