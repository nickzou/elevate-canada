<?php
    $quote_block_args = array(
        'title' => get_field('block_quote')
    );

    get_component('components/block_quote', $quote_block_args);
?>