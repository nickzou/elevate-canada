<section class="container section michelle-obama-tickets-container">
    <div class="row">
        <div class="col col-12">
            <h1 class="section-heading">How to Get Tickets</h1>
        </div>
    </div>
    <?php
        $tickets = get_field('tickets');
        foreach($tickets as $ticket):
            $button = $ticket['button'];
    ?>
        <div class="row">
            <div class="col col-12 col-md-4">
                <a class="featured-image" href="<?php ?>">
                    <img src="<?php echo $ticket['image']['url']; ?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="<?php echo $ticket['image']['url']; ?> 1200w, <?php echo $ticket['image']['sizes']['medium_large']; ?> 768w, <?php echo $ticket['image']['sizes']['medium']; ?> 300w" sizes="(max-width: 1920px) 100vw, 1920px">
                </a>
            </div>
            <div class="col col-12 col-md-5">
                <span class="tickets-subtitle"><?php echo $ticket['subtitle']; ?></span>
                <h2 class="tickets-title"><?php echo $ticket['title']; ?></h2>
                <p class="tickets-content"><?php echo $ticket['content']; ?></p>
                <a class="button red" href="<?php echo $button['link']['url']; ?>">
                    <?php echo $button['text']; ?>
                </a>
            </div>
        </div>
    <?php
        endforeach;
    ?>
</section>