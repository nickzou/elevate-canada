<?php
    if(have_posts()):
        while(have_posts()): the_post();
?>
<section class="container-fluid section hero-section-container">
    <div class="row hero-content-row">
        <div class="container">
            <div class="row">
                <div class="col col-12 col-md-7 hero-info-col">
                    <div class="hero-info-wrapper">
                        <p class="hero-text">Elevate Presents</p>
                        <h1 class="hero-title"><?php the_title(); ?></h1>
                        <h2 class="hero-subtitle"><?php echo get_field('tagline'); ?></h2>
                        <?php
                            $dates = get_field('dates');
                            $date = new DateTime($dates['start_date']);
                        ?>
                        <h3 class="hero-text"><?php echo $date->format('D'); ?>,
                            <?php echo $date->format('M j'); ?> @ <?php echo $date->format('gA') ?><br/>
                            separate ticket required
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-image-wrapper">
        <div class="hero-image-wrapper-inner">
            <?php the_post_thumbnail('full'); ?>
        </div>
    </div>
</section>
<?php
        endwhile;
    endif;
?>