<section class="container-fluid section michelle-obama-profile-container">
    <div class="row">
        <div class="container">
            <?php

                $profile = get_field('speakers')[0];
            
                $profile_args = array(
                    'image' => get_the_post_thumbnail_url($profile->ID),
                    'title' => $profile->post_title,
                    'subtitle' => get_field('subtitle', $profile->ID),
                    'content' => $profile->post_content,
                    'facebook' => get_field('social_media_links', $profile->ID)['facebook'],
                    'twitter' => get_field('social_media_links', $profile->ID)['twitter'],
                    'instagram' => get_field('social_media_links', $profile->ID)['instagram'],
                    'linkedin' => get_field('social_media_links', $profile->ID)['linkedin'],
                    'website' => get_field('social_media_links', $profile->ID)['website']
                );

                get_component('components/speaker_profile', $profile_args);
            
            ?>
        </div>
    </div>
</section>