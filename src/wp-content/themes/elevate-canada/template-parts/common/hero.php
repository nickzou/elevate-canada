<section class="container-fluid section page-hero-container">
    <div class="row">
        <div class="container page-hero-inner-container">
            <div class="row">
                <div class="col col-12">
                    <h1 class="section-heading"><?php echo get_the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <?php
        if(!!the_post_thumbnail()):
            the_post_thumbnail('full');
        else: ?>
        <img width="1920" height="1080" src="<?php echo get_template_directory_uri(); ?>/assets/hero-default-1920px.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="<?php echo get_template_directory_uri(); ?>/assets/hero-default-1920px.jpg 1920w, <?php echo get_template_directory_uri(); ?>/assets/hero-default-1200px.jpg 1200w, <?php echo get_template_directory_uri(); ?>/assets/hero-default-992px.jpg 992w, <?php echo get_template_directory_uri(); ?>/assets/hero-default-768px.jpg 768w, <?php echo get_template_directory_uri(); ?>/assets/hero-default-560px.jpg 560w" sizes="(max-width: 1920px) 100vw, 1920px">
    <?php endif; ?>
</section>