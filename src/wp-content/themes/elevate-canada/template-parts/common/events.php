<section class="container-fluid section events-section">
	<div class="row">
		<div class="col col-12">
			<h1 class="section-heading">Festival Overview</h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-12 p-0">
		  	<div id="events" class="swiper-container front-page-events-slider">
		  		<div class="swiper-wrapper">
					<?php
						$args = array(
							'post_type' => 'events'
						);
						$events = new WP_Query($args);
						if($events->have_posts()):
							while($events->have_posts()): $events->the_post();
					?>
						<div class="swiper-slide">
							<?php

								$card_body_args = array(
									'{date_start}' => get_field('dates')['start_date'],
									'{date_end}' => get_field('dates')['end_date'],
									'{venue}' => get_field('venue'),
									'{post_thumbnail_url}' => get_the_post_thumbnail_url(get_the_ID(), 'medium_large'),
									'{excerpt}' => get_the_excerpt()
								);

								$card_body_template =
								'<div class="event-card-info">
									<div class="event-card-image-wrapper">
										<img class="event-card-featured-image" src="{post_thumbnail_url}"/>
									</div>
									<h2 class="event-card-title">{date_start} - {date_end}</h2>
									<h3 class="event-card-title">{venue}</h3>
									<div class="event-card-excerpt-wrapper">
										<p class="event-card-excerpt">{excerpt}</p>
									</div>
								</div>';

								$card_args = array(
									'title'=> get_the_title(),
									'card_body' => strtr($card_body_template, $card_body_args),
									'button_url' => get_the_permalink(),
									'button_text' => 'learn more'
								);

								get_component('components/card', $card_args);
							?>
						</div>
					<?php
							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</div>
				<button id="events-slider-prev" class="slide-arrow swiper-button-prev">
					<ion-icon name="arrow-back"></ion-icon>
				</button>
				<button id="events-slider-next" class="slide-arrow swiper-button-next">
					<ion-icon name="arrow-forward"></ion-icon>
				</button>
			</div>
		</div>
	</div>
</section>