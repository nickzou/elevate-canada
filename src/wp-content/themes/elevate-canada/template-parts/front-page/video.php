<section class="container-fluid video-section-container">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col col-12 video-col">
                    <div id="video-wrapper" class="video-wrapper">
                        <?php
                            $video = get_field('video');
                        ?>
                        <video id="video" poster="" controls>
                            <source src="<?php echo $video['url']; ?>" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>