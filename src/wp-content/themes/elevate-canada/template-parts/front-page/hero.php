<section class="container-fluid">
	<div class="row">
		<div class="col col-12 p-0">
			<div id="slider" class="swiper-container front-page-hero-slider">
				<div class="swiper-wrapper">
					<?php
					$slider = get_field('slider');
					//var_dump($slider);
					foreach ($slider as $slide):
						//var_dump($slide['image']);
					?>
						<div class="swiper-slide">
							<?php
								echo "<div class='slide-info'>";
								if (!!$slide['title']) {
									echo "<h1 class='slide-title'>" . $slide['title'] . "</h1>";
								}
								if (!!$slide['subtitle']) {
									echo "<h2 class='slide-subtitle'>" . $slide['subtitle'] . "</h2>";
								}
								if (!!$slide['content']) {
									echo "<div class='slide-content'>" . $slide['content'] . "</div>";
								}
								if (!$slide['button']['modal']) {
									echo "<a href='" . $slide['button']['link'] . "' class='button red'>" . $slide['button']['text'] . "</a>";
								} else {
									echo '<button class="button red slide-modal-button" data-modal-id="' . $slide['modal']['video']['id'] .'">' . $slide['button']['text'] . '</button>';
								}
								echo "</div>";
								echo "<img class='slide-image' srcset='" . $slide['image']['sizes']['medium_large'] . " 480w, " . $slide['image']['sizes']['large'] . " 800w, " . $slide['image']['url'] . "' sizes='(max-width: 480px) 480px, (max-width: 768px) 768px, (max-width: 992px) 992px, 1920px' src='" . $slide['image']['url'] . "' />";
							?>
						</div>
					<?php
						endforeach;
					?>
				</div>

				<!-- If we need navigation buttons -->
				<button id="hero-slider-prev" class="slide-arrow swiper-button-prev">
					<ion-icon name="arrow-back"></ion-icon>
				</button>
				<button id="hero-slider-next" class="slide-arrow swiper-button-next">
					<ion-icon name="arrow-forward"></ion-icon>
				</button>
			</div>
		</div>
	</div>
</section>