<section class="container section partners-container">
	<?php
	
		$partner_groups = get_field('partner_groups');

		foreach ($partner_groups as $group):
	
	?>
	<div class="row">
		<div class="col col-12">
			<div class="row">
				<div class="col col-12 text-center">
					<h2 class="section-subheading"><?php echo $group['title']; ?></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<?php
					$partner_logos = $group['partner_logos'];
					foreach ($partner_logos as $logo):
				?>
					<div class="col col-12 col-md-6 col-lg-fifth logos-col">
						<div class="logos-wrapper">
							<h3 class="logo-label"><?php echo $logo['title']; ?></h3>
							<img src="<?php echo $logo['logo']['sizes']['medium']; ?>" />
						</div>
					</div>
				<?php
					endforeach;
				?>
			</div>
		</div>
	</div>
	<?php

		endforeach;

	?>
</section>