<section class="container section main-content-container">
  <div class="row justify-content-center">
        <?php

          if (have_posts()) :

            while (have_posts()) : the_post(); ?>
            <div class="col col-12 col-md-10">
                <?php the_content(); ?>
            </div>
            <?php endwhile;

            else:

            echo '<p>No content found</p>';

          endif;

        ?>
  </div>
</section>