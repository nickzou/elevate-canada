<section class="container section second-content-container">
    <div class="row justify-content-center">
        <div class="col col-12 col-md-10">
            <?php
                $second_content = get_field('second_content');
                echo $second_content;
            ?>
        </div>
    </div>
</section>