<?php

    $slider = get_field('slider');

    foreach ($slider as $slide) {
        if(!!$slide['button']['modal']) {
            $modal_video_args = array(
                'video' => $slide['modal']['video'],
                'id'=> $slide['modal']['video']['id'],
                'url'=> $slide['modal']['video']['url']
            );
        
            get_component('components/modal', $modal_video_args); 
        }
    }