<section class="container section front-page-speakers-section">
	<div class="row">
		<div class="col col-12">
            <h1 class="section-heading">Our 2019 Speakers</h1>
        </div>
	</div>
	<div class="row">
		<?php
			$args = array(
				'post_type' => 'speakers',
				'meta_key' => 'past_speaker',
				'meta_value' => 0,
				'posts_per_page' => '20',
				'orderby' => 'menu_order',
				'order' => 'ASC'
			);
			$current_speakers = new WP_Query($args);
			if($current_speakers->have_posts()):
				while($current_speakers->have_posts()): $current_speakers->the_post();
		?>
			<div class="col col-6 col-md-4 col-lg-fifth">
                <?php
					$speaker_args = array(
						'url'=> get_the_permalink(),
						'logo' => get_field('logo')['sizes']['large'],
						'image' => get_the_post_thumbnail_url(get_the_ID(), 'medium'),
						'name' => get_the_title(),
						'title' => get_field('subtitle'),
						'organization' => get_field('organization')
					);

					get_component('components/speaker_tile', $speaker_args);
				?>
			</div>
		<?php
				endwhile;
			endif;
		?>
    </div>
    <div class="row">
        <div class="col col-12">
            <h1 class="section-heading second-subheading">Our Past Speakers</h1>
        </div>
    </div>
    <div class="row">
        <?php
			$args = array(
				'post_type' => 'speakers',
				'meta_key' => 'past_speaker',
				'meta_value' => 1,
				'posts_per_page' => '10',
				'orderby' => 'menu_order',
				'order' => 'ASC'
			);
			$past_speakers = new WP_Query($args);
			if($past_speakers->have_posts()):
				while($past_speakers->have_posts()): $past_speakers->the_post();
		?>
			<div class="col col-6 col-md-4 col-lg-fifth">
				<?php
					$speaker_args = array(
						'url'=> get_the_permalink(),
						'logo' => get_field('logo')['sizes']['large'],
						'image' => get_the_post_thumbnail_url(get_the_ID(), 'medium'),
						'name' => get_the_title(),
						'title' => get_field('subtitle'),
						'organization' => get_field('organization')
					);

					get_component('components/speaker_tile', $speaker_args);
				?>
			</div>
		<?php
				endwhile;
            endif;
            wp_reset_postdata();
		?>
	</div>
	<div class="row">
		<div class="col col-12 d-flex justify-content-center">
			<?php 
				$speakers_button = get_field('speakers_button');
				if(!!$speakers_button):
			?>
			<a class="button red" href="<?php echo $speakers_button['link']['url']; ?>">
				<?php echo $speakers_button['text']; ?>
			</a>
			<?php
				endif;
			?>
		</div>
	</div>
</section>