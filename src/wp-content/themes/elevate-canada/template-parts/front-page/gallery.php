<section class="container section gallery-container">
	<div class="row">
		<div class="col col-12 p-0">
			<div id="gallery" class="swiper-container front-page-gallery-slider">
				<div class="swiper-wrapper">
					<?php
						$gallery = get_field('gallery');
						foreach ($gallery as $slide):
					?>
						<div class="swiper-slide">
							<div class="slide-image-container">
								<img class="slide-image" srcset="<?php echo $slide['image']['sizes']['medium_large'] . " 480w, " . $slide['image']['sizes']['large'] . " 800w, " . $slide['image']['url']; ?>" src="<?php echo $slide['image']['url']; ?>">
							</div>
							<h1 class="slide-title"><?php echo $slide['title']; ?></h1>
						</div>
					<?php
						endforeach;
					?>
				</div>
				<button id="gallery-slider-prev" class="slide-arrow swiper-button-prev">
					<ion-icon name="arrow-back"></ion-icon>
				</button>
				<button id="gallery-slider-next" class="slide-arrow swiper-button-next">
					<ion-icon name="arrow-forward"></ion-icon>
				</button>
			</div>
		</div>
	</div>
</section>