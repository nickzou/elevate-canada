<section class="container section page-content-container">
    <div class="row">
        <div class="col col-12">
            <?php the_content(); ?>
        </div>
    </div>
</section>