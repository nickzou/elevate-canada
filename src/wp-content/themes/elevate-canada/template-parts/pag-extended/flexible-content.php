<section class="container section page-flexible-content-container">
<?php

if( have_rows('content_blocks') ):

    // loop through the rows of data
   while ( have_rows('content_blocks') ) : the_row();

   ?>

        <div class="row">

   <?php

       if( get_row_layout() == 'titled_content' ):

           the_sub_field('title');
           the_sub_field('text');

    //    elseif( get_row_layout() == 'download' ): 

    //        $file = get_sub_field('file');

       endif;

    ?>

        </div>

    <?php

   endwhile;

else :

   // no layouts found

endif;

?>
</section>