<section class="container page-content-container">
    <div class="row">
        <?php
        
            if (have_posts()):

                while(have_posts()): the_post();

        ?>
        <div class="col col-12">
            <?php the_content(); ?>
        </div>
        <?php
        
                endwhile;

            endif;
        
        ?>
    </div>
</section>