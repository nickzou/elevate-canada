<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title(); ?></title>
    <!--Mailchimp Base Style-->
    <style type="text/css">
	#mc_embed_signup{background:#transparent; clear:left; font:16px Helvetica,Arial,sans-serif; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
    </style>
    <?php wp_head();?>
</head>
<body <?php body_class(); ?>>
    <div class="loader page-loader">
        <div class="spinner">
        </div>
    </div>
    <header id="site-header" class="site-header">
        <div class="container-fluid desktop-header-container">
            <div class="row">
                <div class="col col-9 col-lg-4 site-logo-col">
                    <?php
                        get_template_part('template-parts/site-logo');
                    ?>
                </div>
                <div id="main-menu" class="col col-12 col-lg-8 main-menu-col">
                    <?php
                        $header_menu_args = array(
                            'theme_location' => 'header-menu'
                        );
                        wp_nav_menu($header_menu_args);
                    ?>
                </div>
            </div>
        </div>
        <button id="mobile-menu-button" class="hamburger hamburger--slider js-hamburger mobile-menu-button" type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>
    </header>