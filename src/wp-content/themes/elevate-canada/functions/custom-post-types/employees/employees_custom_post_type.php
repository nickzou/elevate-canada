<?php
    function employees_custom_post_type() {
        $labels = array(
            'name'               => _x( 'Employees', 'post type general name' ),
            'singular_name'      => _x( 'Employee', 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New Employee' ),
            'edit_item'          => __( 'Edit Employee' ),
            'new_item'           => __( 'New Employee' ),
            'all_items'          => __( 'All Employees' ),
            'view_item'          => __( 'View Employee' ),
            'search_items'       => __( 'Search Employees' ),
            'not_found'          => __( 'No Employees found' ),
            'not_found_in_trash' => __( 'No Employees found in the Trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'Employees'
        );
        $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'rewrite' => array('slug' => 'employees', 'with_front' => true),
        'query_var' => true,
        'menu_icon' => 'dashicons-businesswoman',
        'supports' => array(
            'title',
            'editor',
            'custom-fields',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',),
        'show_in_rest' => true,
        'rest_base' => 'employees',
        'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type( 'employees', $args );
    }
    add_action( 'init', 'employees_custom_post_type' );