<?php
    function events_custom_post_type() {
        $labels = array(
            'name'               => _x( 'Events', 'post type general name' ),
            'singular_name'      => _x( 'Event', 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New Event' ),
            'edit_item'          => __( 'Edit Event' ),
            'new_item'           => __( 'New Event' ),
            'all_items'          => __( 'All Events' ),
            'view_item'          => __( 'View Event' ),
            'search_items'       => __( 'Search Events' ),
            'not_found'          => __( 'No Events found' ),
            'not_found_in_trash' => __( 'No Events found in the Trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'Events'
        );
        $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'rewrite' => array('slug' => 'events', 'with_front' => false),
        'query_var' => true,
        'menu_icon' => 'dashicons-calendar-alt',
        'supports' => array(
            'title',
            'editor',
            'custom-fields',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',),
        'show_in_rest' => true,
        'rest_base' => 'events',
        'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type( 'events', $args );
        add_post_type_support( 'events', 'excerpt' );
    }
    add_action( 'init', 'events_custom_post_type' );