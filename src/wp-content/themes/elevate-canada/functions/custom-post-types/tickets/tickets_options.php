<?php

    if(function_exists('acf_add_options_page')) {
        acf_add_options_sub_page(array(
            'page_title' => 'Tickets Options',
            'menu_title' => 'Tickets Options',
            'parent_slug' => 'edit.php?post_type=tickets'
        ));
    }