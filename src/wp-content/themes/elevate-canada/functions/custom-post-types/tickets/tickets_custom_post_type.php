<?php
    function tickets_custom_post_type() {
        $labels = array(
            'name'               => _x( 'Tickets', 'post type general name' ),
            'singular_name'      => _x( 'Ticket', 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New Ticket' ),
            'edit_item'          => __( 'Edit Ticket' ),
            'new_item'           => __( 'New Ticket' ),
            'all_items'          => __( 'All Tickets' ),
            'view_item'          => __( 'View Ticket' ),
            'search_items'       => __( 'Search Tickets' ),
            'not_found'          => __( 'No Tickets found' ),
            'not_found_in_trash' => __( 'No Tickets found in the Trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'Tickets'
        );
        $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'rewrite' => array('slug' => 'tickets', 'with_front' => true),
        'query_var' => true,
        'menu_icon' => 'dashicons-tickets-alt',
        'supports' => array(
            'title',
            'editor',
            'custom-fields',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',),
        'show_in_rest' => true,
        'rest_base' => 'tickets',
        'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type( 'tickets', $args );
        add_post_type_support( 'tickets', 'excerpt' );
    }
    add_action( 'init', 'tickets_custom_post_type' );