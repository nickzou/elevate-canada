<?php

    if(function_exists('acf_add_options_page')) {
        acf_add_options_sub_page(array(
            'page_title' => 'Speakers Options',
            'menu_title' => 'Speakers Options',
            'parent_slug' => 'edit.php?post_type=speakers'
        ));
    }