<?php
    function speakers_custom_post_type() {
        $labels = array(
            'name'               => _x( 'Speakers', 'post type general name' ),
            'singular_name'      => _x( 'Speaker', 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New Speaker' ),
            'edit_item'          => __( 'Edit Speaker' ),
            'new_item'           => __( 'New Speaker' ),
            'all_items'          => __( 'All Speakers' ),
            'view_item'          => __( 'View Speaker' ),
            'search_items'       => __( 'Search Speakers' ),
            'not_found'          => __( 'No Speakers found' ),
            'not_found_in_trash' => __( 'No Speakers found in the Trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'Speakers'
        );
        $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'rewrite' => array('slug' => 'speakers', 'with_front' => true),
        'query_var' => true,
        'menu_icon' => 'dashicons-businessperson',
        'supports' => array(
            'title',
            'editor',
            'custom-fields',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',),
        'show_in_rest' => true,
        'rest_base' => 'speakers',
        'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type( 'speakers', $args );
    }
    add_action( 'init', 'speakers_custom_post_type' );