<?php

    function enqueue_global_resources() {
        wp_enqueue_script('ion-icons');
        wp_enqueue_style('mailchimp-css');
        wp_enqueue_style('styles');
        wp_enqueue_script('app');
        wp_enqueue_style('stickyfooter-styles');
        wp_enqueue_script('stickyfooter-js');
        if(is_page() && !is_front_page()) {
            wp_enqueue_style('page-styles');
            wp_enqueue_script('page-js');
        }
        if(is_front_page()) {
            wp_enqueue_style('frontpage-styles');
            wp_enqueue_script('frontpage-js');
        }
        if(is_page_template('templates/template-tickets.php')) {
            wp_enqueue_style('tickets-styles');
            wp_enqueue_script('tickets-js');
            wp_enqueue_script('tito-js');
            wp_enqueue_style('tito-styles');
        }
        if(is_singular('events')) {
            wp_enqueue_style('events-styles');
            wp_enqueue_script('events-js');
        }
        if(is_post_type_archive('events')) {
            wp_enqueue_style('events-archive-styles');
            wp_enqueue_script('events-archive-js');
        }
        if(is_singular('speakers')) {
            wp_enqueue_style('speakers-styles');
            wp_enqueue_script('speakers-js');
        }
        if(is_page_template('templates/template-michelle-obama.php')) {
            wp_enqueue_style('event-michelle-styles');
            wp_enqueue_script('event-michelle-js');
        }
    }

    add_action('wp_enqueue_scripts', 'enqueue_global_resources');