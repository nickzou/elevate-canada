<?php
    function theme_setup() {
        register_nav_menus(
            array(
                'header-menu' => __('Header Menu'),
                'footer-1'=>__('Footer Menu 1'),
                'footer-2'=>__('Footer Menu 2'),
                'footer-3'=>__('Footer Menu 3'),
                'footer-4'=>__('Footer Menu 4')
            )
        );

        add_theme_support('post-thumbnails');
    }

    add_action('after_setup_theme', 'theme_setup');