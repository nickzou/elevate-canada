<?php

    function custom_excerpt_more ($more) {
        if(is_main_query()) {
            return ' <a class="read-more" href=' . get_the_permalink() . '>Read More</a>';
        } 
    }

    add_filter('excerpt_more', 'custom_excerpt_more');