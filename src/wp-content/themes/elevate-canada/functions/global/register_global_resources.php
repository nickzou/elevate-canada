<?php

    function register_global_resources() {
        wp_register_script('ion-icons', 'https://unpkg.com/ionicons@4.5.10-1/dist/ionicons.js', array(), '1', true);
        wp_register_style('mailchimp-css', '//cdn-images.mailchimp.com/embedcode/classic-10_7.css', array(), '1', 'all' );
        wp_register_script('mailchimp-js', '//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', array(), '1', false);
        wp_register_script('app', get_template_directory_uri() . '/js/app.js', array(), '1', true);
        wp_register_style('styles', get_template_directory_uri() . '/css/styles.css', array(), '1.0.0', 'all');
        wp_register_script('stickyfooter-js', get_template_directory_uri() . '/js/stickyfooter.js', array(), '1', true);
        wp_register_style('stickyfooter-styles', get_template_directory_uri() . '/css/stickyfooter.css', array(), '1.0.0', 'all');
        wp_register_script('page-js', get_template_directory_uri() . '/js/page.js', array(), '1', true);
        wp_register_style('page-styles', get_template_directory_uri() . '/css/page.css', array(), '1.0.0', 'all');
        wp_register_script('frontpage-js', get_template_directory_uri() . '/js/frontpage.js', array(), '1', true);
        wp_register_style('frontpage-styles', get_template_directory_uri() . '/css/frontpage.css', array(), '1.0.0', 'all');
        wp_register_script('tickets-js', get_template_directory_uri() . '/js/tickets.js', array(), '1', true);
        wp_register_style('tickets-styles', get_template_directory_uri() . '/css/tickets.css', array(), '1.0.0', 'all');
        wp_register_script('events-js', get_template_directory_uri() . '/js/events.js', array(), '1', true);
        wp_register_style('events-styles', get_template_directory_uri() . '/css/events.css', array(), '1.0.0', 'all');
        wp_register_script('event-michelle-js', get_template_directory_uri() . '/js/event-michelle.js', array(), '1', true);
        wp_register_style('event-michelle-styles', get_template_directory_uri() . '/css/event-michelle.css', array(), '1.0.0', 'all');
        wp_register_script('events-archive-js', get_template_directory_uri() . '/js/events-archive.js', array(), '1', true);
        wp_register_style('events-archive-styles', get_template_directory_uri() . '/css/events-archive.css', array(), '1.0.0', 'all');
        wp_register_script('speakers-js', get_template_directory_uri() . '/js/speakers.js', array(), '1', true);
        wp_register_style('speakers-styles', get_template_directory_uri() . '/css/speakers.css', array(), '1.0.0', 'all');
        wp_register_script('tito-js', 'https://js.tito.io/v1', array(), '1.0.0', true);
        wp_register_style('tito-styles', 'https://css.tito.io/v1.1', '1.0.0', 'all');
    }

    add_action('init', 'register_global_resources');