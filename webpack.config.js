const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const wordpressInstall = require('./wordpressInstall');

var babelLoader = {test: /\.js$/, loader: 'babel-loader', exclude:/node_modules/};
var rawLoader = {test: /\.html$/, loader: 'raw-loader', exclude: /node_modules/};
var styleLoader = {test: /\.css$/, loader: "style-loader!css-loader", exclude: /node_modules/};
var scssLoader = {test: /\.scss$/, loader: "style-loader!css-loader?sourceMap!postcss-loader?sourceMap!sass-loader?sourceMap", exclude: /node_modules/};
var scssLoaderExtracted = {test: /\.scss$/, use: ExtractTextPlugin.extract({
  fallback: "style-loader",
  use: "css-loader?sourceMap!postcss-loader?sourceMap!sass-loader?sourceMap"
}), exclude: /node_modules/};
var urlLoader = {test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/, loader: 'url-loader'};

var defaultModuleRules = [
  babelLoader,
  rawLoader,
  urlLoader
];

var pathsToClean = [
  __dirname + wordpressInstall + '/wp-content/themes/elevate-canada',
];

var extractedCSSModuleRules = defaultModuleRules.slice();
extractedCSSModuleRules.push(styleLoader, scssLoaderExtracted);

var config = {
  watch: true,
  devtool: 'source-map'
};

var generalConfig = Object.assign({}, config, {
  name: "config",
  entry:{  app: ['@babel/polyfill', __dirname + "/src/entry/app.js"]},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/app.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new CleanWebpackPlugin(pathsToClean),
    new ExtractTextPlugin('css/styles.css'),
    new CopyWebpackPlugin([
        { from: __dirname + '/src/wp-content', to:  __dirname + wordpressInstall + '/wp-content/' }
    ])
  ],
});

var stickyFooterConfig = Object.assign({}, config, {
  name: "stickyFooterConfig",
  entry:{  app: __dirname + "/src/entry/stickyfooter.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/stickyfooter.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/stickyfooter.css')
  ],
});

var pageConfig = Object.assign({}, config, {
  name: "pageConfig",
  entry:{  app: __dirname + "/src/entry/page.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/page.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/page.css')
  ],
});

var frontPageConfig = Object.assign({}, config, {
  name: "frontPageConfig",
  entry:{  app: __dirname + "/src/entry/frontpage.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/frontpage.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/frontpage.css')
  ],
});

var eventsSingleConfig = Object.assign({}, config, {
  name: "eventsSingleConfig",
  entry:{  app: __dirname + "/src/entry/events.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/events.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/events.css')
  ],
});

var eventMichelleConfig = Object.assign({}, config, {
  name: "eventMichelleConfig",
  entry:{  app: __dirname + "/src/entry/event-michelle.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/event-michelle.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/event-michelle.css')
  ],
});

var eventsArchiveConfig = Object.assign({}, config, {
  name: "eventsArchiveConfig",
  entry:{  app: __dirname + "/src/entry/events-archive.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/events-archive.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/events-archive.css')
  ],
});

var ticketsComponentConfig = Object.assign({}, config, {
  name: "ticketsComponentConfig",
  entry:{  app: __dirname + "/src/entry/tickets.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/tickets.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/tickets.css')
  ],
});

var speakersSingleConfig = Object.assign({}, config, {
  name: "speakersSingleConfig",
  entry:{  app: __dirname + "/src/entry/speakers.js"},
  output: {
    path: __dirname + "/src/wp-content/themes/elevate-canada",
    filename: "js/speakers.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/speakers.css')
  ],
});

module.exports = [
  generalConfig, stickyFooterConfig, pageConfig, frontPageConfig, eventsSingleConfig, eventMichelleConfig, eventsArchiveConfig, ticketsComponentConfig, speakersSingleConfig
];
